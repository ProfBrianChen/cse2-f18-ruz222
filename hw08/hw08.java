//Name:Runjie Zhao
//CSE002-Homework 8
//Make a deck of card and shuffle and get a hand of cards

//import Scanner class
import java.util.Scanner;
public class hw08 {

	public static void main(String []args)
	{
		Scanner input = new Scanner(System.in);
		//Make a list of card's headname
		String []Name = {"C","H","S","D"};
		//Make a list of card's number
		String []Card = {"2","3","4","5","6","7","8","9","10","J","Q","K","A"};
		//Set the length of the cards
		String []cards = new String[52];
		//Set the value in the cards
		for(int i = 0; i < cards.length; i++)
		{
			cards[i] = Card[i % 13] + Name[i / 13];
		}
		//Show the value in the cards
		printArray(cards);
		//Use shuffle to change the cards order
		cards = shuffle(cards);
		//Ask the user to input the number of hand cards
		System.out.println("Please enter how many cards you want");
		
		int numCards = input.nextInt();
		
		int temp = numCards;
		
		int index = 51;
		
		//To calculate whether the length is over the original length
		while(temp > cards.length)
		{
			index += 51;
			
			temp -= cards.length;
		}
		
		//System.out.println(numCards);
		
		int repeat = 0;
		
    //To calculate if number of the cards is more than the deck of cards
		while(repeat == 0)
		{
			String hand[] = getHand(cards,index,numCards);
			
			index -= numCards;
			
			printArray(hand);
			
			if(hand.length < numCards) {
				
				System.out.println("You do not have enough cards");
				
				break;
			}
			
			System.out.println("If you want another hand, please enter 0");
			
			repeat = input.nextInt();
		}
	}
	
  //Create a method to print the Array
	public static void printArray(String list[]) {
		//Use for loop to print out the content in list
		for(int i = 0; i < list.length;i++)
		{
			System.out.print(list[i] + " ");
		}
		
		System.out.println();
	}
	
  //Create a method to shuffle the cards
	public static String[] shuffle(String list[])
	{
		//Use for loop to shuffle the cards
		for(int i = 1; i < list.length; i++)
		{
			int index = (int)(Math.random()*52);
			
			//Exchange the value
			String temp = list[index];
			
			list[index] = list[0];
			
			list[0] = temp;
		}
		
		for(int i = 0; i < list.length;i++)
		{
			//Print out the value
			System.out.print(list[i] + " ");
		}
		
		System.out.println();
		
		return list;
	}
	
  
  //Create a method to the cards in hand
	public static String[] getHand(String list[], int index, int numCards) {
		String newlist[];
		
		newlist = new String[index];
		
		for(int i = 0; i < newlist.length; i++)
		{
			newlist[i] = list[i % 52];
		}
		
		if(index < numCards)
		{
			String []hand = new String[index];
			
			for(int i = 0; i < hand.length; i++)
			{
				hand[i] = newlist[index-1];
				index--;
			}
			
			
			return hand;
		}
		
		String []hand = new String[numCards];
		
		//Pass value to hand list
		for(int i = 0; i < hand.length; i++)
		{
			hand[i] = newlist[index-1];
			index--;
		}
		
		return hand;
	}
}

