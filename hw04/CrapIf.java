//Name: Runjie Zhao
//CSE002----Homework2
//To compile a classic casino game only use the if
//statement
//Import Scanner class to this program
import java.util.Scanner;

public class CrapIf
{
  public static void main(String []args)
  {
    //Create a variable of the Scanner class
    Scanner input = new Scanner(System.in);
    
    //Ask the typer whether to type into the number or use the random number
    System.out.println("Do you want to use the number you give or random number?");
    System.out.println("1.If you want to choose number you give please type 1");
    System.out.println("2.If you want to choose random number please type 2");
    
    //Initialize the choice integer number
    int choice = input.nextInt();
    
    //Initializethe sum of two numbers
    int sum=0;
    
    //Initialize whether two numbers are the same
    boolean judge=false;
    
    //judge the type if it is given number
    if(choice == 1)
    {
      //ask the typer to type in the first number
      System.out.println("Please enter the first number from one to six");
      
      //initialize the first variable
      int firstnum = input.nextInt();
      
      //ask the typer to type in the second number
      System.out.println("Please enter the second number from one to six");
      
      //Initialzie the second variable
      int secondnum = input.nextInt();
      
      //Judge the number whether they are in the range
      if(firstnum > 6 || secondnum > 6 || firstnum < 0 || secondnum < 0)
      {
        System.out.println("The number is not in the range");
      }else{
        //Calculate the sum of two given numbers
        sum = firstnum + secondnum;
        
        //Initialize the boolean to judge whether two numbers are t=he same or not
        judge = firstnum == secondnum;
        
        //Judge the sum and judge and then generate the correct cas=ino
        
      }
    }
    
    //Judge which method the typer wants if it is random number
    if(choice == 2)
    {
      int random1 = (int)(Math.random() * 6 + 1);
    
      //Initialize the second random number
      int random2 = (int)(Math.random() * 6 + 1);
    
      //Initialize the sum of random1 and random2
      sum = random1 + random2;
    
      //Initialize the boolean to judge whether two number are same
      judge = (random1 == random2);
      
      //Show two numbers
      System.out.println("The first number is " + random1);
      System.out.println("The second number is " + random2);
      
    }
    
    //Judge the sum and the number then  generate the correct casino
    if(sum == 2 && judge)
    {
      System.out.println("It's Snake Eyes");
    }else if(sum == 3 && !judge)
    {  
      System.out.println("It's Ace Deuce");
    }else if(sum == 4 && !judge)
    {
      System.out.println("It's Easy Four");
    }else if(sum == 4 && judge)
    {
      System.out.println("It's Hard Four");
    }else if(sum == 5)
    {
      System.out.println("It's Fever Five");
    }else if(sum == 6 && !judge)
    {
      System.out.println("It's Easy Six");
    }else if(sum == 6 && judge)
    {
      System.out.println("It's Hard Five");
    }else if(sum == 7)
    {
      System.out.println("It's Seven Out");
    }else if(sum == 8 && !judge)
    {
      System.out.println("It's Easy Eight");
    }else if(sum == 8 && judge)
    {
      System.out.println("It's Hard Eight");
    }else if(sum == 9)
    {
      System.out.println("It's Nine");
    }else if(sum == 10 && !judge)
    {
      System.out.println("It's Easy Ten");
    }else if(sum == 10 && judge)
    {
      System.out.println("It's Hard Ten");
    }else if(sum == 11)
    {
      System.out.println("It's Yo-leven");
    }else if(sum == 12)
    {
      System.out.println("It's Boxcars");
    }
     
    //Initialize the first random number
    
  }
}