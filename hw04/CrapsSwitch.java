//Name: Runjie Zhao
//Email: ruz222@lehigh.edu
//CSE002----HW04
//Use switch method to complie the casino game
//Import the scanner class
import java.util.Scanner;

public class CrapsSwitch
{
  public static void main(String []args)
  {
    //Initialize a variable in the Scanner class
    Scanner input = new Scanner(System.in);
    
    //Ask the typer whether to type into the number or use the random number
    System.out.println("Do you want to use the number you give or random number?");
    System.out.println("1.If you want to choose number you give please type 1");
    System.out.println("2.If you want to choose random number please type 2");
    
    //Initialize the choice integer number
    int choice = input.nextInt();
    
    //Initialize the sum of two numbers
    int sum=0;
    
    //Initialize two numbers that will be generated
    int firstnum = 0; 
    int secondnum = 0;
    
    //Initialize whether two numbers are the same
    boolean judge=false;
    
    //Use switch method to judge the number the typer type in
    switch(choice)
    {
      //If the typer choose to type in  
      case 1:
        //ask the typer to type in the first number
        System.out.println("Please enter the first number from one to six");
      
        //initialize the first variable
        firstnum = input.nextInt();
      
        //ask the typer to type in the second number
        System.out.println("Please enter the second number from one to six");
      
        //Initialzie the second variable
        secondnum = input.nextInt();
        
        break;
        
      //If the typer choose to use random numbers  
      case 2:
        //Randomize two numbers
        firstnum = (int)(Math.random() * 6 + 1);
        secondnum = (int)(Math.random() * 6 + 1);
        break;
        
      default:
        System.out.println("You should choose between one and two");
        break;
        
    }
    
    //Use switch sentence to judge the right casino
    switch(firstnum)
    {
      //When the first number is 1
      case 1:
        switch(secondnum)
        {
          case 1://When the second number is 1
            System.out.println("It's Snake Eyes");
            break;
          case 2://When the second number is 2
            System.out.println("It's Ace Deuce");
            break;
          case 3://When the second number is 3
            System.out.println("It's Easy Four");
            break;
          case 4://When the second number is 4
            System.out.println("It's Fever Five");
            break;
          case 5://When the second number is 5
            System.out.println("It's Easy Six");
            break;
          case 6://When the second number is 6
            System.out.println("It's Seven Out");
            break;
          default://When the second number not in the range
            System.out.println("The number is out of range");
            break;
        }
        break;
        //When the first number is 2
      case 2:
        switch(secondnum)
        {
          case 1://When the second number is 1
            System.out.println("It's Ace Deuce");
            break;
          case 2://When the second number is 2
            System.out.println("It's Hard Four");
            break;
          case 3://When the second number is 3
            System.out.println("It's Fever Five");
            break;
          case 4://When the second number is 4
            System.out.println("It's Fever Five");
            break;
          case 5://When the second number is 5
            System.out.println("It's Seven Out");
            break;
          case 6://When the second number is 6
            System.out.println("It's Easy Eight");
            break;
          default://When the second number not in the range
            System.out.println("The number is out of range");
            break;
        }
        break;
        //When the first number is 3
      case 3:
        switch(secondnum)
        {
          case 1://When the second number is 1
            System.out.println("It's Easy Four");
            break;
          case 2://When the second number is 2
            System.out.println("It's Fever Five");
            break;
          case 3://When the second number is 3
            System.out.println("It's Hard Six");
            break;
          case 4://When the second number is 4
            System.out.println("It's Seven Out");
            break;
          case 5://When the second number is 5
            System.out.println("It's Easy Eight");
            break;
          case 6://When the second number is 6
            System.out.println("It's Nine");
            break;
          default://When the second number not in the range
            System.out.println("The number is out of range");
            break;
        }
        break;
        //When the first number is 4
      case 4:
        switch(secondnum)
        {
          case 1://When the second number is 1
            System.out.println("It's Fever Five");
            break;
          case 2://When the second number is 2
            System.out.println("It's Easy Six");
            break;
          case 3://When the second number is 3
            System.out.println("It's Seven Out");
            break;
          case 4://When the second number is 4
            System.out.println("It's Hard Eight");
            break;
          case 5://When the second number is 5
            System.out.println("It's Nine");
            break;
          case 6://When the second number is 6
            System.out.println("It's Easy Ten");
            break;
          default://When the second number not in the range
            System.out.println("The number is out of range");
            break;
        }
        break;
       //When the first number is 5
      case 5:
        switch(secondnum)
        {
          case 1://When the second number is 1
            System.out.println("It's Easy Six");
            break;
          case 2://When the second number is 2
            System.out.println("It's Seven Out");
            break;
          case 3://When the second number is 3
            System.out.println("It's Easy Eight");
            break;
          case 4://When the second number is 4
            System.out.println("It's Nine");
            break;
          case 5://When the second number is 5
            System.out.println("It's Hard Ten");
            break;
          case 6://When the second number is 6
            System.out.println("It's Yo-leven");
            break;
          default://When the second number not in the range
            System.out.println("The second number is out of range");
            break;
        }
        break;
        //When the first number is 6
      case 6:
        switch(secondnum)
        {
          case 1://When the second number is 1
            System.out.println("It's Seven Out");
            break;
          case 2://When the second number is 2
            System.out.println("It's Easy Eight");
            break;
          case 3://When the second number is 3
            System.out.println("It's Nine");
            break;
          case 4://When the second number is 4
            System.out.println("It's Easy Ten");
            break;
          case 5://When the second number is 5
            System.out.println("It's Yo-leven");
            break;
          case 6://When the second number is 6
            System.out.println("It's Boxcars");
            break;
          default://When the second number not in the range
            System.out.println("The number is out of range");
            break;
        }
        break;
        //When it is not in the range from one to six
      default:
        System.out.println("The number is out of range");
        break;
    }
  }
}