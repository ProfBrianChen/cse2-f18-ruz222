//Name: Runjie Zhao
//CSE002
//HW02
//This program is to calculate the cost of each kind of products and the tax of them
//Display the each product's cost, tax and, total cost at the end of the program
public class Arithmetic
{
  public static void main(String []args)
  {
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;

    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;

    //the tax rate
    double paSalesTax = 0.06;
    
    double totalCostOfPants;    //total cost of pants
    double totalCostOfShirts;   //total cost of shirts
    double totalCostOfBelts;    //total cost of belts
    double taxOfPants;          //the tax of pants
    double taxOfShirts;         //the tax of shirts
    double taxOfBelts;          //the tax of belts
    double totalCostOfPurchase; //the total cost of pants, shirts, and belts
    double totalTax;            //the total tax cost of pants, shirts, and belts
    double totalPaid;           //the total money the customer should pay for the seller
    
    //total cost of pants only leave two digits after the decimal
    totalCostOfPants = (double)(int)(numPants * pantsPrice * 100)/100;
    //show the cost of pant
    System.out.println("The cost of pants is " + totalCostOfPants);
    
    //total cost of shirts only leave two digits after the decimal
    totalCostOfShirts = (double)(int)(100 * numShirts * shirtPrice)/100;
    //show the cost of shirts
    System.out.println("The cost of shirts is " + totalCostOfShirts);
    
    //total cost of belts only leave two digits after the decimal
    totalCostOfBelts = (double)(int)(100 * numBelts * beltCost)/100;
    //show the cost of belts
    System.out.println("The cost of belts is " + totalCostOfBelts);
    
    //sales tax charged by each kind of item
    //As shown below
    //tax in pants
    taxOfPants = (double)(int)(100 * paSalesTax * totalCostOfPants)/100;
    //Show the tax in pants
    System.out.println("The tax in the pants is " + taxOfPants);
    
    //tax in shirts
    taxOfShirts = (double)((int)(100 * paSalesTax * totalCostOfShirts))/100;
    //Show the result of the tax in shirts
    System.out.println("The tax in the shirts is " + taxOfShirts);
    
    //tax in belts
    taxOfBelts = (int)(100 * paSalesTax * totalCostOfBelts)/(double)100;
    //show tax in belts
    System.out.println("The tax in the belts is " + taxOfBelts);
    
    //total cost of purchase
    totalCostOfPurchase = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
    //Show the total cost of the purchase
    System.out.println("The cost of purchasing the pants, shirts and belts is " + totalCostOfPurchase);
    
    //total cost of tax
    totalTax = (double)(int)((taxOfPants + taxOfShirts + taxOfBelts) * 100)/100;
    //Show the tax needed to pay for the purchase
    System.out.println("The tax needed to pay for the purchase is " + totalTax);
    
    //totalpaid including tax
    totalPaid = (double)(int)(100 * (totalTax + totalCostOfPurchase))/100;
    //Show the total money needed to pay including the tax
    System.out.println("The total money needed to pay for the shirts and the pants including the tax is " + totalPaid);
  }
}