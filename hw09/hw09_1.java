//Name:Runjie Zhao
//CSE002--HW09_1
//Purpose:To compile the program and find the element in it using binary search and linear search
import java.util.Scanner;

public class hw09_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Set up a variable of scanner
		Scanner input = new Scanner(System.in);
		
		System.out.println("Enter 15 ascending ints for final grades in CSE2");
		
		int grades[] = new int[15];

		for(int i = 0; i < 15; i++)
		{
			
			System.out.println("Please enter student" + (i + 1) + "'s score");
			
			while(!input.hasNextInt())
			{
				System.out.println("Please enter the score again");
				
				input.next();
			}
			
			int score = input.nextInt();
			//Test whether the array is increasing and in the range. Also, we have to avoid the situation when i = 0
			while((i != 0 && score < grades[i - 1]) || (score < 0 || score > 100))
			{
				
				System.out.println("Please enter the score again it may out of limits");
				
				while(!input.hasNextInt())
				{
					System.out.println("Please enter the score again what you enter is not an integer");
					
					input.nextLine();
				}
				
				score = input.nextInt();
			
			}
			
			grades[i] = score;
			
		}
		
		Print(grades);
		
		System.out.print("Please enter the search key: ");
		
		int key = input.nextInt();
		
		Binary_Search(grades, key);
		
		Scramble(grades);
		
		System.out.println("Scrambled");
		
		Print(grades);
		
		System.out.print("Please enter the search key: ");
		
		key = input.nextInt();
		
		Linear_Search(grades,key);
		
	}
	
	public static void Scramble(int arr[]) {
		
		for(int i = 0; i < arr.length; i++)
		{
			
			int temp = arr[(int)(Math.random() * 15)];
			
			arr[(int)(Math.random() * 15)] = arr[0];
			
			arr[0] = temp;
			
		}
		
	}
	//Set up a linear search to find the key in the array arr
	public static void Linear_Search(int []arr, int key) {
		
    int times = 0;
    
		int index = -1;
		
		for(int i = 0; i < arr.length; i++)
		{
      
			times++;
      
			if(arr[i] == key)
			{
				
				index = i;
				
				break;
				
			}
			
		}
		
		if(index == -1)
		{
			
			System.out.println(key + " was not found in the list with " + times + " iterations");
		
		}else {
			
			System.out.println(key + " was found in index " + index + " with " + times + " iterations");
			
		}
		
	}
	//Set up a binary search to find the key in the array arr
	public static void Binary_Search(int []arr, int key) {
		
		int high = arr.length - 1;
		
		int low = 0;
		
		int index = -1;
		
		int times = 0;
		
		while(high >= low)
		{
			//System.out.println(high + " " + low);
			if(key == arr[(high + low) / 2])
			{
				
				index = (high + low) / 2;
				
				break;
				
			}else if(key < arr[(high + low) / 2])
			{
				
				high = (high + low) / 2 - 1;
				
			}else if(key > arr[(high + low) / 2])
			{
				
				low = (high + low) / 2 + 1;
				
			}
			
			times++;
			
		}
		
		if(index == -1) {
			
			System.out.println(key + " was not found in the list with " + times + " iterations");
			
		}else {
			
			System.out.println(key + " was found in the list in the index " + index + " with " + times + " iterations");
			
		}
		
	}
	
	//Set up a method to print out the array
	public static void Print(int arr[]) {
		
		for(int i = 0; i < arr.length; i++)
		{
			
			System.out.print(arr[i] + " ");
			
		}
		
		System.out.println();
		
	}

}
