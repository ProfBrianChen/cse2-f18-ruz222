//Name: Runjie Zhao
//CSE002-HW09_2
//Purpose: Use random method to inpur the value to each element in the array
//Find whether there is a target value and delete it
import java.util.Scanner;

public class hw09_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan=new Scanner(System.in);
		int num[]=new int[10];
		int newArray1[];
		int newArray2[];
		int index,target;
		    String answer="";
		    do{
		      System.out.print("Random input 10 ints [0-9]");
		      num = randomInput(num);
		      String out = "The original array is:";
		      out += listArray(num);
		      System.out.println(out);
		 
		      System.out.print("Enter the index ");
		      index = scan.nextInt();
		      newArray1 = delete(num,index);
		      String out1="The output array is ";
		      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
		      System.out.println(out1);
		 
		      System.out.print("Enter the target value ");
		      target = scan.nextInt();
		      newArray2 = remove(num,target);
		      String out2="The output array is ";
		      out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
		      System.out.println(out2);
		       
		      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
		      answer=scan.next();
		    }while(answer.equals("Y") || answer.equals("y"));
		  }
		 
	public static String listArray(int num[]){
		    String out="{";
		    for(int j=0;j<num.length;j++){
		      if(j>0){
		        out+=", ";
		      }
		      out+=num[j];
		    }
		    out+="} ";
		    return out;
		  }
	//Create a method to randomly input the number into the array
	public static int[] randomInput(int arr[]) {
		
		for(int i = 0; i < arr.length; i++)
		{
			
			arr[i] = (int)(Math.random() * 10);
			
		}
		
		return arr;
		
	}
	//Delete the number from the position
	public static int[] delete(int arr[], int pos) {
		//Check whether pos satisfies the prerequisite
		if(pos > arr.length - 1 || pos < 0)
		{
			System.out.println("The index is not valid");
			
			return arr;
		}
		
		int arr1[] = new int[arr.length - 1];
		//time is responsible for the index in array arr
		int time = 0;
		//Use the for loop to find the element in the position
		for(int i = 0; i < arr.length; i++)
		{
			
			if(i == pos)
			{
				continue;
			}
			
			arr1[time] = arr[i];
			
			time++;
		}
		
		return arr1;
		
	}
	//Remove the number where the elemnet is equal to the target
	public static int[] remove(int []arr, int target){
		
		int length = 0;
		
		boolean judge = false;
		
		for(int i = 0; i < arr.length; i++)
		{
			
			if(target == arr[i]) {
				System.out.println("Element " + target + " has been found");
				
				judge = true;
				
				break;
			}
			
		}
		
		if(!judge) {
			
			System.out.println("Element " + target + " was not found");
			
		}
		
		for(int i = 0; i < arr.length; i++)
		{
			
			if(arr[i] != target)
			{
				length++;
			}
			
		}
		
		int arr1[] = new int[length];
		
		int time = 0;
		
		for(int i = 0; i < arr.length; i++)
		{
			
			if(arr[i] != target)
			{
				
				arr1[time] = arr[i];
				
				time++;
				
			}
		
		}
		
		return arr1;
		
	}
	
}
