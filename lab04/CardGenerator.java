//Name: Runjie Zhao
//CSE002----Lab04
//To generator a random number then use the random
//number to present a card
public class CardGenerator
{
  public static void main(String args[])
  {
    //Use random method to generate a random number
    int randomCard = (int)(Math.random() * 52 + 1);
    
    //Use judge to judge the kind of card(diamonds, clubs, hearts, spades)
    String suit=null, identity;
    
    //Use if statement to assign the suit of the card
    if(randomCard >= 1 && randomCard <= 13)
    {
      //Show the suit as the Diamonds
      suit = "Diamonds";
    }else if(randomCard >= 14 && randomCard <= 26)//When it comes to clubs
    {
      //Show the suit as the club
      suit = "Clubs";
    }
    else if(randomCard >= 27 && randomCard <= 39)//When it comes to hearts
    {
      //Show the suit as the hearts
      suit = "Hearts";
    }else if(randomCard >= 40 && randomCard <=52)
    {
      //Show the suit as the spades
      suit = "Spades";
    }
    switch(randomCard%13)
      {
        case 1:
          identity = "Ace";
          //Show the result of the random card
          System.out.println("You picked the " + identity + " of " + suit);
          break;
        case 11:
          identity = "Jack";
          //Show the result of the random card
          System.out.println("You picked the " + identity + " of " + suit);
          break;
        case 12:
          identity = "Queen";
          //Show the result of the random card
          System.out.println("You picked the " + identity + " of " + suit);
          break;
        case 0:
          identity = "King";
          //Show the result of the random card
          System.out.println("You picked the " + identity + " of " + suit);
          break;
        default:
          //Show the result of the random card
          System.out.println("You picked the " + (randomCard % 13) + " of " + suit);
          break;
  }
}
}