//Name: Runjie Zhao
//CSE002
//Tranfer the unit and calculate the volume of the raindrops in the affected area.
//impotr Scanner class
import java.util.Scanner;

public class Convert
{
  //main method required for every java program
  public static void main(String []args)
  {
    //Set up a variable of the scanner class
    Scanner input = new Scanner(System.in);
    
    //ask the writer to type in the acres
    System.out.print("Enter the affected area in acres: ");
    
    //Set up a double variable to store the number that typer writes
    double acres = input.nextDouble();
    
    //ask the writer to type in the rainfall in the affected area(inches)
    System.out.print("Enter the rainfall in the affected area: ");
    
    //Set up a double variable to store the number that typer writes
    double inches = input.nextDouble();
    
    //Set up the volume variable for conversion
    double Milevolume;
    
    //The transfer from acres to square inches
    double Acretrans = 1.5624989 * Math.pow(10,-3);
    
    //The transfer from inch to miles
    double Inchtrans = 1.57828 * Math.pow(10,-5);
    
    //Transfer gallon into Cubic Miles
    Milevolume = acres * Acretrans * inches * Inchtrans;
    
    //Show the result in cubic inches form
    System.out.println( Milevolume + " cubic miles");
  }//end of main method
} //end of class