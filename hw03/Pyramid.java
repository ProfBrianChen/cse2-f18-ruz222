//Name: Runjie Zhao
//CSE002 hw#3
//To calculate the volume of a pyramid
//Import the Scanner class
import java.util.Scanner;

public class Pyramid
{
  //main method for every java program
  public static void main(String []args)
  {
    //Set up a new variable of Scanner
    Scanner input = new Scanner(System.in);
    
    //Ask the typer to input the variable of the length
    System.out.print("The square side of the pyramid is (input length): ");
    
    //Set up the variable length to store the value
    double length = input.nextDouble();
    
    //Ask the typer to input the variable of the height
    System.out.print("The height of the pyramid is (input height): ");
    
    //Set up the variable height to store the value
    double height = input.nextDouble();
    
    //Set up the volume variable
    double volume;
    
    //Calculate the volume by using the formula l^2 * h * (1/3)
    volume = Math.pow(length,2) * height * 1 / 3;
    
    //Display the volume
    System.out.println("The volume inside the pyramid is: " + volume);
  }//end of main method
}//end of the class