//Name: Runjie Zhao
//CSE002----Homework5
//To get the 5 cards from 52 cards
//Judge the kinds of these five cards and find the probability
//impoer scanner class
import java.util.Scanner;

public class Hw05{
  public static void main(String []args){
    //Define the variable of a scanner class
		Scanner input = new Scanner(System.in);
				
		//Ask the typer to input the times to generate the hands
		System.out.println("Please enter the times to generate the hands");
		int times = input.nextInt();
		
		//This is for counting the loop times
		int number = 0;
				
		//Define the five cards
		int card1, card2, card3, card4, card5;
		
		//Change the card to value
		int val1, val2, val3, val4, val5;
				
		//Define the times of each situation
		int optime = 0, twotime = 0, threetime = 0, fourtime = 0;
		
		//Boolean a value to judge during while statement
		boolean judge = true;
				
		//Use the loop to calculate the times
		while(number < times)
		{
			//Suppose the first card number
			card1 = (int)(Math.random() * 52 + 1);
			
			//Change the value
			val1 = card1 % 13;
					
			//Suppose the second card number
			card2 =(int)(Math.random() * 52 + 1);
			while(card1 == card2)
			{
				card2 = (int)(Math.random() * 52 + 1);
			}
					
			//Change the value
			val2 = card2 % 13;
					
					
			//Suppose the third number
			card3 = (int)(Math.random() * 52 + 1);
			while(card1 == card3 || card2 == card3) 
			{
				card3 = (int)(Math.random() * 52 + 1);
			}
					
			//Change to value
			val3 = card3 % 13;
			
			//Suppose the fourth number*/
			card4 = (int)(Math.random() * 52 + 1);
			while(card1 == card4 || card2 == card4 || card3 == card4) 
			{
				card4 = (int)(Math.random() * 52 + 1);
			}
					
			//Change to value
			val4 = card4 % 13;
					
			//Suppose the fifth card number
			card5 = (int)(Math.random() * 52 + 1);
			while(card1 == card5 || card2 == card5 || card3 == card5 || card4 == card5) 
			{
				card5 = (int)(Math.random() * 52 + 1);
			}
					
			//Change to value
			val5 = card5 % 13;
					
			
			//To judge the kinds
			if(val1 == val2)
			{
				if(val1 == val3)
				{
					if(val1 == val4)
					{
						//whether it's four kind
						fourtime++;
					}else if(val1 == val5) {
						//whether it's four kind
						fourtime++;
					}
					else{
						//whether it's three kind
						threetime++;
						if(val4 == val5)
						{
							//whether it's a pair
							optime++;
						}
					}
				}else if(val1 == val4){
					if(val4 == val5)
					{
						//whether it's four kind
						fourtime++;
					}else {
						//whether it's three kind
						threetime++;
						if(val3 == val5)
						{
							//whether it's a pair
							optime++;
						}
					}
				}else if(val1 == val5) {
					//whether it's three kind
					threetime++;
					if(val4 == val3)
					{
						//whether it's a pair
						optime++;
					}
				}else {
					if(val3 == val4 && val4 == val5)
					{
						//whether it's a house
						threetime++;
						optime++;
					}else if(val3 == val4 && val3 != val1 || val5 == val4 && val4 != val1 || val3 == val5 && val3 != val1 ) {
						//whether it's two pairs which are different values
						twotime++;
					}else {
						//whether it's a pair
						optime++;
					}
				}
			}else if(val1 == val3) {
				      if(val1 == val4)
				{
						if(val1 == val5) {
						//whether it's four kind
						fourtime++;
					}else {
						//whether it's three kind
						threetime++;
						if(val2 == val5)
						{
							//whether it's a pair
							optime++;
						}
					}
				}else if(val1 == val5) {
					//whether it's three kind
					threetime++;
					if(val4 == val2)
					{
						//whether it's a pair
						optime++;
					}
				}else {
					if(val2 == val4 && val4 == val5) {
						//whether it's a house
						threetime++;
						optime++;
					}else if(val2 == val4 && val2 != val1 || val5 == val4 && val4 != val1 || val2 == val5 && val2 != val1 ) {
						//whether it's a two pairs with different values
						twotime++;
					}else {
						//whether it's a pair
						optime++;
					}
				}
			}else if(val1 == val4){
				if(val4 == val5)
				{
					//whether it's a three kind
					threetime++;
					if(val2 == val3)
					{
						//whether it's a pair
						optime++;
					}
				}else {
					if(val2 == val3 && val3 == val5) {
						//whether it's a house
						threetime++;
						optime++;
					}else if(val2 == val3 && val2 != val1 || val5 == val3 && val4 != val1 || val2 == val5 && val2 != val1 ) {
						//whether it's two pairs with different values
						twotime++;
					}else {
						//whether it's a pair
						optime++;
					}
				}
			}else if(val1 == val5) {
				if(val2 == val3 && val4 == val3) {
					//whether it's a house
					threetime++;
					optime++;
				}else if(val2 == val4 && val2 != val1 || val3 == val4 && val4 != val1 || val2 == val3 && val2 != val1 ) {
					//whether it's two pairs with different values
					twotime++;
				}else {
					//whether it's a pair
					optime++;
				}
			}
			else if(val2 == val3) //To judge the value difference without val1 == val4 condition
			{
				//To judge val2 is equal to val4
				if(val2 == val4)
				{
					if(val2 == val5)
					{
						//whether it's four kind
						fourtime++;
					}else {
						//whether it's three kind
						threetime++;
					}
							
				}else if(val2 == val5)
				{
					//whether it's three kind
					threetime++;
				}else {
					if(val4 == val5 && val2 != val5){
						//whether it's two pairs of different values
						twotime++;
					}else {//with it's a pair
						optime++;
					}
				}
			}else if(val2 == val4){
				if(val2 == val5) {
					//whether it's three kind
					threetime++;
				}else {
					if(val3 == val5 && val3 != val2) {
						//whether it's two pairs of different values
						twotime++;
					}else{
						//whether it's a pair
						optime++;
					}
				}
			}else if(val2 == val5){
				if(val3 == val4 && val3 != val2)
				{
					//whether it's two pair of different values
					twotime++;
				}else {
					//whether it's a pair
			     	optime++;
				}
			}else if(val3 == val4)//To judge the value difference without val2 == val4
			{
				if(val3 == val5)
				{
					//whether it's a three kind
					threetime++;
				}else {
					//whether it's a pair
					optime++;
				}
			}else if(val3 == val5) {
				//whether it's a pair
				optime++;
			}else if(val4 == val5) {
				//whether it's pair
				optime++;
			}
					
			//for while judge action after each iteration
			++number;
		}
				
				
		//define the probability
		double onePro = (int)(double)((optime / (double)times) * 1000) / 1000.0 ;
		double twoPro = (int)((double)twotime / times * 1000) / 1000.0;
		double threePro = (int)((double)threetime / times * 1000) / 1000.0;
		double fourPro = (int)((double)fourtime / times * 1000 )/ 1000;
				
		//Show the results with three decimal numbers
		System.out.printf("The number of loops: (user inputted integer) %d\n" ,times);
		System.out.printf("The probability of Four-of-a-kind: %.3f\n" , fourPro);
		System.out.printf("The probabilit of Three-of-a-kind: %.3f\n" , threePro);
		System.out.printf("The probability of Two-pair: %.3f\n" , twoPro);
		System.out.printf("The probability of One-pair: %.3f\n" , onePro);
  }
}