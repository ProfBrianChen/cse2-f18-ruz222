//Name: Runjie Zhao
//CSE002
//Lab03
//Split the check evenly after having dinner with friends
//import the scanner class
import java.util.Scanner;

public class Check
{
  //main method required for every java program
  public static void main(String []args)
  {
    //Set up a variable of Scanner class
    Scanner myScanner = new Scanner(System.in);
    
    //ask the typer to type the cost of the check
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    
    //Set up a variable to store the value of the input
    double checkCost = myScanner.nextDouble();
    
    //ask the typer to type the percentage of the tip
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    
    //Set up the variable to store the value of the input
    double tipPercent = myScanner.nextDouble();
    
    //To convert to the percentage form
    tipPercent /= 100;
    
    //ask the typer to type the number of the people who went out to dinner
    System.out.print("Enter the number of people who went out to dinner: ");
    
    //Set up the variable to store the value of the input
    int numPeople = myScanner.nextInt();
    
    double totalCost;
    double costPerPerson;
    int dollars,   //whole dollar amount of cost 
    dimes, pennies; //for storing digits
    //to the right of the decimal point 
    //for the cost$ 
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    //get the whole amount, dropping decimal fraction
    dollars = (int)costPerPerson;
    //get dimes amount, e.g., 
    // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
    //  where the % (mod) operator returns the remainder
    //  after the division:   583%100 -> 83, 27%5 -> 2 
    dimes = (int)(costPerPerson * 10) % 10;
    pennies = (int)(costPerPerson * 100) % 10;
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);

  }//end of main method
}//end of class