import java.text.*;

public class Deci
{
  public static void main(String []args)
  {
    L l= new L(9.98);
    System.out.println("The result is "+l.Area());
  }
}

class L
{
  double a;
  DecimalFormat dd=new DecimalFormat("0.00");
  L(double length)
  {
    a=length;
  }
  double Area()
  {
    return Double.parseDouble(dd.format(a));
  }
}