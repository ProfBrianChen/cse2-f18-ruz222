//Name: Runjie Zhao
//Email:ruz222@lehigh.edu
//CSE HW 01 WelcomeClass
public class WelcomeClass
{
  public static void main(String []args)
  {
    //Prints the first line which needs two space bars at the beginning
    System.out.println("  -------------");
    //Prints WELCOME at the second lien
    System.out.println("  |  WELCOME  |");
    //Prints the third line which also needs two space bars at the beginning
    System.out.println("  -------------");
    //Prints the arrows with two bars between each
    System.out.println("  ^  ^  ^  ^  ^  ^");
    //Prints the slashes with one space bar between each, because \ means data link escape character, so add one more \ after \
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    //Print our my Lehigh ID card
    System.out.println("<-R--U--Z--2--2--2->");
    //Prints the slashes with one space bar between each
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    //Print v with two space bars between each
    System.out.println("  v  v  v  v  v  v");
    /*
    Autobiographic statement: This is the homework from Runjie Zhao.
    I still have one question why if I use google server to run the code above, the final 
    result will be messed up, but if I use internet explorer, my result will be what I want?
    */
  }
}