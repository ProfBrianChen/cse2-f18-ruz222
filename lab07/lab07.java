//Name: Runjie Zhao
//CSE002--lab07
import java.util.Random;
import java.util.Scanner;

public class lab07
{
  public static void main(String []args)
  {
    Scanner input = new Scanner(System.in);
		System.out.print("The ");
		for(int i = 0; i < 6; i++)
		{
			if(i < 2)
			{
				System.out.print(Adj() + " ");
			}else if(i == 2)
			{
				System.out.print(ObNoun() + " ");
			}else if(i ==  3)
			{
				System.out.print(PastVerb() + " ");
			}else if(i == 4)
			{
				System.out.print("the " + Adj() + " ");
			}else if(i == 5){
				System.out.print(SubNoun() + "\n");
				System.out.println("Do you want another sentence?");
				boolean judge = input.nextBoolean();
				if(judge) {
					i = -1;
				}
			}
		}
		
		Combine();
  }
  public static String Adj()
	{
		Random randomGenerator = new Random();
		int randomInt = randomGenerator.nextInt(10);
		
		switch(randomInt)
		{
		case 1:
			return "beautiful";
		case 2:
			return "happy";
		case 3:
			return "fast";
		case 4:
			return "religious";
		case 5:
			return "quick";
		case 6:
			return "huge";
		case 7:
			return "tasty";
		case 8:
			return "wonderful";
		case 9:
			return "tough";
		default:
			return "awful";
		}
	}
		
		public static String SubNoun()
		{
			Random randomGenerator = new Random();
			int randomInt = randomGenerator.nextInt(10);
			
			switch(randomInt)
			{
			case 1:
				return "dog";
			case 2:
				return "fox";
			case 3:
				return "fish";
			case 4:
				return "cat";
			case 5:
				return "monster";
			case 6:
				return "dragon";
			case 7:
				return "headset";
			case 8:
				return "cow";
			case 9:
				return "fly";
			default:
				return "lion";
			}
		}
			
			public static String ObNoun()
			{
				Random randomGenerator = new Random();
				int randomInt = randomGenerator.nextInt(10);
				
				switch(randomInt)
				{
				case 1:
					return "human";
				case 2:
					return "robot";
				case 3:
					return "hunter";
				case 4:
					return "Pokemon";
				case 5:
					return "computer";
				case 6:
					return "RPG";
				case 7:
					return "earphone";
				case 8:
					return "cowboy";
				case 9:
					return "flyer";
				default:
					return "King";
				}
			}
			
			public static String PastVerb()
			{
				Random randomGenerator = new Random();
				int randomInt = randomGenerator.nextInt(10);
				
				switch(randomInt)
				{
				case 1:
					return "killed";
				case 2:
					return "ate";
				case 3:
					return "shoot";
				case 4:
					return "caught";
				case 5:
					return "slained";
				case 6:
					return "taught";
				case 7:
					return "controlled";
				case 8:
					return "verified";
				case 9:
					return "proved";
				default:
					return "formed";
				}
			}
			
			public static String firstSentence()
			{
				String output = " ";
				System.out.print("The ");
				for(int i = 0; i < 6; i++)
				{
					if(i < 2)
					{
						System.out.print(Adj() + " ");
					}else if(i == 2)
					{
						System.out.print(ObNoun() + " ");
					}else if(i ==  3)
					{
						System.out.print(PastVerb() + " ");
					}else if(i == 4)
					{
						System.out.print("the " + Adj() + " ");
					}else if(i == 5){
						output = SubNoun();
						System.out.print(output + "\n");
					}
				}
				return output;
			}
			
			public static void secondSentence(String sth)
			{
				System.out.print("This ");
				for(int i = 0; i < 5; i++)
				{
					if(i == 0)
					{
						System.out.print(sth + " was ");
					}else if(i == 1 || i == 3)
					{
						System.out.print(Adj() + " ");
					}else if(i == 2)
					{
						System.out.print(PastVerb() + " to ");
					}else {
						System.out.print(SubNoun() + "\n");
					}
				}
				
				System.out.println("It used " + ObNoun() + " to " + PastVerb() + " " + ObNoun() + " at the " + Adj() + " " + sth);
			}
			
			public static void ConclusionSentence(String conclusion)
			{
				System.out.print("That " + conclusion + " " + PastVerb() + " his " + SubNoun() + "!" + "\n");
			}
			
			public static void Combine()
			{
				String a = firstSentence();
				secondSentence(a);
				ConclusionSentence(a);
			}
}