//Name: Runjie Zhao
//CSE002----Method Homework


import java.util.Scanner;

public class HW07 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
    //Use text method to input the text
		String text = Text();
		//Ask the user to choose the option from the menu
		String choice = printMenu();
    //To judge whether the value is true or not
		while(true) {
		  while(!choice.equals("c")&&!choice.equals("w")&&!choice.equals("f")&&!choice.equals("r")&&!choice.equals("s")&&!choice.equals("q"))
		  {
			  System.out.println("Please enter your choice again");
			  choice = printMenu();
		  }
		
		  //Judge the value and then show the method
		  if(choice.equals("c"))
		  {
			
			  getNumOfNonWSCharacters(text);
		  }else if(choice.equals("w"))
		  {
		  	getNumOfWords(text);	
		  }else if(choice.equals("f"))
		  {
		  	Scanner input = new Scanner(System.in);
		  	
		  	System.out.println("Enter a phrase to be found");
			
		  	String want = input.next();
			
		  	findText(want, text);
		  }else if(choice.equals("r"))
		  {
		  	text = replaceExclamation(text);
		  	System.out.println(text);
		  }else if(choice.equals("s"))
		  {
			  text = shortenSpace(text);
			  System.out.println(text);
		  }else if(choice.equals("q"))
		  {
			  System.out.println("Quit");
			  break;
		  }
		  choice = printMenu();
		}
	}
	
  //Method text asks the user to input the String
	public static String Text()
	{
		Scanner input = new Scanner(System.in);
		
		System.out.println("Enter a sample text");
		
		String text = input.nextLine();
		
		return text;
	}
	
  //Printmenu method that asks the user to choose an option
	public static String printMenu()
	{
		System.out.println("Menu\nc - Number of non-whitespace characters\nw - Number of words\nf - Find text\nr - Replace all !'s\ns - Shorten spaces\nq - Quit");
		System.out.println("Choose an option: ");
		
		Scanner input = new Scanner(System.in);
		
		String choice = input.next();
		
		while(true)
		{
			if(choice.equals("c") && choice.equals("w") && choice.equals("f") && choice.equals("r") && choice.equals("s") && choice.equals("q"))
			{
				System.out.println("Your enter is wrong please enter again");
				
				choice = input.next();
			}else {
				break;
			}
		}//end of the loop to judge whether it's the acceptable value
		return choice;
	}
	
  //Get the number of the character except the spacebars
	public static void getNumOfNonWSCharacters(String text)
	{
		int length = text.length();
		
		int times = 0;
		
		for(int i = 0; i < length; i++)
		{
			if(text.charAt(i)!=' ')
			{
				times++;
			}
		}
		
		System.out.println("Number of non-whitespace characters: " + times);
	}
	
  //Get the number of the words method except getting the spacebars
	public static void getNumOfWords(String text)
	{
		int length = text.length();
		
		int times = 1;
		
		for(int i = 0; i < length; i++)
		{
			if(i != length - 1 && text.charAt(i) == ' '&& text.charAt(i + 1) != ' ')
			{
				times++;
			}
		}
		
		System.out.println("Number of words " + times);
	}
	
  //Find the wanted text in the sample text
	public static void findText(String want, String text)
	{
		int length = text.length();
			
		int times = 0;
		
		for(int i = 0; i < length; i++)
		{
			if(text.charAt(i) == want.charAt(0))
			{
				int  k = 0;
				
				boolean judge = false;
				
				for(int j = i; j - i < want.length(); k++,j++)
				{
					if(text.charAt(j) != want.charAt(k))
					{
						judge = false;
						break;
					}else {
						judge = true;
					}
				}
				
				if(judge == true)
				{
					times++;
				}
			}
		}
		
		System.out.println("\""+ want + "\"" + " instances: " + times);
	}
	
  
  //Replace the exclamation in the sample method and return the text
	public static String replaceExclamation(String text)
	{
		int length = text.length();
		
		int times = 1;
		
		int position = 0;
		
		String change = "";
		
		for(int i = 0; i < length; i++)
		{
			if(text.charAt(i) == '!')
			{
				if(times == 1 && i == 0) {
					change = ".";
					position = i + 1;
					times++;
				}else if(times == 1){
					change = text.substring(0, i) + ".";
					times++;
					position = i + 1;
				}else {
					change += text.substring(position, i) + ".";
					position = i + 1;
				}
			}
		}
		
		if(position <= text.length() - 1)
		{
			change += text.substring(position);
		}
		
		return change;
	}
	
	
	//Shorten the space bar in the text and then return the text
	public static String shortenSpace(String text)
	{
		String change = " ";
		
		int counts = 0;
		
		int times = 1;
		
		int position = 0;
		
    //Use the position to find the place after the spacebar to avoid more than two spacebars
		for(int i = 0; i < text.length(); i++)
		{
			if(i + 1 != text.length() && text.charAt(i) == ' ' && text.charAt(i + 1) == ' ' && times == 1 && i == 0)
			{
				for(int j = i; j < text.length(); j++)
				{
					if(text.charAt(j) != ' ')
					{
						break;
					}
					
					counts++;
				}
				
				change = " ";
				times++;
				position = i + counts;
				i = position;
				
				counts = 0;
			}else if(i + 1 != text.length() && text.charAt(i) == ' ' && text.charAt(i + 1) == ' ' && times == 1 )
			{
			
				for(int j = i; j < text.length(); j++)
				{
					if(text.charAt(j) != ' ')
					{
						break;
					}
					
					counts++;
				}
				
				change = text.substring(0, i) + " ";
				
				times++;
				
				position = i + counts;
				
				i = position;
				
				counts = 0;
			}else if(i + 1 != text.length() && text.charAt(i) == ' ' && text.charAt(i + 1) == ' ' ){
				for(int j = i; j < text.length(); j++)
				{
					if(text.charAt(j) != ' ')
					{
						break;
					}
					
					counts++;
				}
				
				change += text.substring(position,i) + " ";
				
				position = i + counts;
				
				i = position;
				
				counts = 0;
			}
		}
		
		if(position < text.length())
		{
			change += text.substring(position);
		}
		
		return change;
	}
	
	
	
}