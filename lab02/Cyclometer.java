//Name: Runjie Zhao
//CSE002
//This program is to measure the time, distance and counts
public class Cyclometer
{
  //main method required for every Java program
  public static void main(String []args)
  {
    int secsTrip1=480;  //This variable is for the number of seconds of the No.1 trip
    int secsTrip2=3220;  //This variable is for the number of seconds of the No.2 trip
    int countsTrip1=1561;  //This varibale is for the rotation of the No.1 trip
    int countsTrip2=9037; //This variable is for the rotation of the No.2 trip
    double wheelDiameter=27.0,  //This variable is for the distance of the wheel
    PI=3.14159, //
    feetPerMile=5280,  //This is the tranfer between feet and mile
    inchesPerFoot=12,   //This is the transfer between inches and foot
    secondsPerMinute=60;  //This is the transfer between seconds and minute
    double distanceTrip1, distanceTrip2,totalDistance;  //Define the value for the distance of trip one, two and the combination between one and two
    System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts."); //Show the minutes and counts in trip one
    System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");//Show the minutes and counts in trip two
    distanceTrip1=countsTrip1*wheelDiameter*PI;// To count and distance in trip by multiply the perimeter of the round by number of counts in trip one
    // Above gives distance in inches
    //(for each count, a rotation of the wheel travels
    //the diameter in inches times PI)
    distanceTrip1/=inchesPerFoot*feetPerMile; // Give the distances in mile
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;//this form is to combine the two formulas above together
    totalDistance=distanceTrip1+distanceTrip2;//To calculate the total distance by adding distance in trip one and distance in trip two together
    //Print out the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");
  }// end of main method
}// end of class