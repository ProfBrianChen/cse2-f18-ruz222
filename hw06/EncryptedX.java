//Name: Runjie Zhao
//CSE002---HW06
//Print X with asteriks
import java.util.Scanner;

public class EncryptedX {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Set up the variable of class Scanner
		Scanner input = new Scanner(System.in);
		
		//Ask the user to type in a number
		System.out.print("Please enter a number from 0 to 100: ");
		
		//To test whether the number is an integer
		while(!input.hasNextInt())
		{
			System.out.println("Please enter an integer!");
			input.next();
		}

		//Give the number the value
		int number = input.nextInt();
		
		
		//To judge whether the number is between 0 and 100
		while(number >= 100 || number < 0)
		{
			System.out.println("The number should between 0 and 100");
			number = input.nextInt();
		}
		
		//Use for loop to form the picture
		for(int i = 1; i <= number + 1; i++) {
			for(int j = 1; j <= number + 1; j++)
			{
				if(i == j && (number + 2 - i) != i)
				{
					//find the place where needs the spacebar
					System.out.print(" ");
				}else if(j == number + 2 - i)
				{
					//find the place where needs the spacebar
					System.out.print(" ");
				}else
				{
					//find the place where needs the asterik
					System.out.print("*");
				}
			}
			//Move to the next line
			System.out.println();
		}
		
	}

}
