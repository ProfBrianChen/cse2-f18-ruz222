//Name: Runjie Zhao
//CSE002--Lab06
import java.util.Scanner;
public class lab06
{
  public static void main(String []args)
  {
    Scanner input =  new Scanner(System.in);
		
		System.out.println("Please enter a number");
		
		int number = input.nextInt();
		
		while(number > 10 || number < 1)
		{
			System.out.println("Please type in a number that is in the range of 1 to 10");
			number = input.nextInt();
		}
		
		//System.out.println(number);
		
		System.out.println("Pattern A");
		
		for(int i = 0; i < number ; i++)
		{
			for(int j = 0; j <= i ; j++)
			{
				System.out.print( ( j + 1 ) + " ");
			}
			System.out.println("");
		}
		
		System.out.println("");		
		
		System.out.println("Pattern B");
		
		for(int i = 0; i < number ; i++)
		{
			for(int j = 1; j <= number - i ; j++)
			{
				System.out.print( j + " ");
			}
			System.out.println("");
		}
		
        System.out.println("");		
		
		System.out.println("Pattern C");
		
		for(int i = 0; i < number ; i++)
		{
			for(int j = 1; j <= number - i - 1 ; j++)
			{
				System.out.print(" ");
			}
			
			for(int k = 0; k <= i ; k++)
			{
				System.out.print(number - (number - i + k) + 1);
			}
			
			System.out.println("");
		}
		
		System.out.println("");
		
		System.out.println("Pattern D");
		
		for(int i = 0; i < number; i++)
		{
			for(int k = 1; k <= number - i; k++)
			{
				System.out.print(number - k + 1 + " ");
				
			}
			System.out.println("");
		}
  }
}